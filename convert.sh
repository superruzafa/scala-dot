#!/bin/bash

for d in *.dot
do
	file=`basename -s .dot $d`
	dot $d -Tpng -o $file.png
done
